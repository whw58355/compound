import { Env, getEntry } from "./src/scripts/webpack/helper"
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
import { Configuration } from "webpack"
import { resolve } from "path"
import base from "./webpack.config.base"
const CleanWebpackPlugin = require("clean-webpack-plugin")

// origin "[name].bundle.js" , chunk file name for share module
base.output.filename = "[name].[chunkhash].main.bundle.js"
base.output.chunkFilename = "[name].[chunkhash].share.bundle.js"

export default async (env: Env | undefined) => {
  const entry = await getEntry(env)

  const config: Configuration = {
    ...base,
    entry: entry,

    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          include: resolve(__dirname, "src"),
          use: [
            {
              loader: "ts-loader",
              options: {
                configFile: "tsconfig.webpack.json"
              }
            }
          ]
        },

        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                importLoader: 2
              }
            },
            "sass-loader"
          ]
        },
        {
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          loader: "url-loader?limit=30000&name=[name]-[hash].[ext]"
        }
      ]
    },

    // optimization: {
    //   splitChunks: {
    //     cacheGroups: {
    //       commons: {
    //         test: /[\\/]node_modules[\\/]/,
    //         name: true,
    //         chunks: "all"
    //       }
    //     }
    //   }
    // },

    plugins: [
      new MiniCssExtractPlugin({
        filename: "[name].[contenthash].css",
        chunkFilename: "[name].[contenthash].css"
      }),
      new CleanWebpackPlugin(["static/build/bundle/*.*"], { watch: false })
    ]
  }

  return config
}
