const obj = {
  apps: [
    // First application
    {
      name: "compound",

      script: "./dist/server/server/server.js",

      env: {
        NODE_ENV: "production"
      }
    }
  ]
};

module.exports = obj;
