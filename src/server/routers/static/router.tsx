import * as Router from "koa-router"
import * as send from "koa-send"
import * as React from "react"
const router = new Router()
import { tmpdir } from "os"
//serve static file

// public
router.get("/public/:dir/:id", async ctx => {
  await send(ctx, ctx.path, { root: __dirname + "../../../../../static" })
})

router.get("/build/:dir/:id", async ctx => {
  await send(ctx, ctx.path, { root: __dirname + "../../../../../static" })
})

router.get("/favicon.ico", async ctx => {

  await send(ctx, ctx.path, { root: __dirname + "../../../../../static/public/favicon" })
})

// server single image
router.get("/api/images/:dirName/:id", async ctx => {
  const { origin } = ctx.query
  const { dirName, id } = ctx.params
  const originArr = origin.split(".")
  const extension = id.split(".").pop()
  // remove last element
  originArr.pop()

  ctx.attachment(originArr.join(".") + "." + extension)
  await send(ctx, `/${dirName}/images/${id}`, { root: tmpdir() + "/store" })


})

// serve zip
router.get("/api/zip/:dirName/:id", async ctx => {
  const { dirName, id } = ctx.params
  ctx.attachment("compress-image.net.zip")
  await send(ctx, `/${dirName}/${id}`, { root: tmpdir() + "/store" })

})

export { router }
