import * as Router from "koa-router"
import * as React from "react"
import { readdir } from "fs"
import { promisify } from "util"
import { join } from "path"
const router = new Router()
import { ILang } from "../../../pages/index/langs/interface"
import {Context} from "koa"

const handler = async (ctx:Context) => {

  let { langName } = ctx.params
  
  if(!langName){
  langName = "cn"
  }
  const { lang } = await import(`../../../pages/index/langs/${langName}`)
  const { title, description, lang: htmlLang } = (lang as ILang).head
  let langString = `<script>window.lang=${JSON.stringify(lang)}</script>`
  if (langName === "en") {
    langString = ""
  }

  let src = ""
  let href = ""
  const dirs = await promisify(readdir)(join(__dirname + "../../../../../static/build/bundle"))

  dirs.forEach((name: string) => {
    const url = `/build/bundle/${name}`

    if (/\.js$/.test(name)) src = url

    if (/\.css$/.test(name)) href = url
  })

  // only for production, let generate:html know ,and inject ga script
  const isProduction = process.env.NODE_ENV === "production"? `<script id="is-production"></script>`:""


  const html = `<!DOCTYPE html>
<html lang="${htmlLang}">
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
    <link rel="stylesheet" href="${href}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="${description}" >
</head>
<body>
<div id="root"></div>
<div>${langString}</div>
<script id="main-script" src="${src}"></script>
${isProduction}
</body>
</html>`

  ctx.body = html


}

router.get("/",handler)

router.get("/:langName",handler)

export { router }
