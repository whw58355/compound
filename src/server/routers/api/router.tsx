import * as Router from "koa-router"
import { tmpdir } from "os"
import { join } from "path"
const router = new Router()
import { Context } from "koa"
import * as mkdir from "make-dir"
import * as shortId from "shortid"
import { Files } from "formidable"
import { IOptions, Data } from "../../../pages/index/store/interaface"
import { imageProcessor, getUniqueNameImages, IImages } from "./helper"
import { createReadStream, createWriteStream, readdir } from "fs"
import * as archiver from "archiver"
import { promisify } from "util"

router.post("/api/compress", async (ctx: Context) => {

  // files attach with koa body
  const files: Files = (ctx.request as any).files

  // my file
  const file = files.file

  //  get extension of this file
  const extension = file.name.split(".").pop()

  // file name to save, do not want - _ at begin or the end
  const fileId = "i" + shortId.generate() + "d"

  // get options from client
  const options: IOptions = ctx.query

  const dirName = options.id

  //writeFileDir /tmp/image/:dirName/
  const outputDir = join(tmpdir(), "store", dirName, "images")
  // make sure the dir is exist
  await mkdir(outputDir)

  // limit the number of images
  const imageNumber = await promisify(readdir)(outputDir)
  if (imageNumber.length >= 20) {
    ctx.status = 500
    ctx.body = { msg: "20 images limit." }
    return
  }

  // let image process to handle everything ,then await info
  const { infoPromise, fileIdName } = imageProcessor(options, file, fileId, extension, outputDir)
  const info = await infoPromise
  const data: Data = { info, fileIdName, dirName }
  ctx.body = data



})

// create zip
router.post("/api/zip/:dir", async ctx => {
  const { dir } = ctx.params
  const images: any[] = (ctx.request as any).body.images

  const uniqueNameImages = getUniqueNameImages(images)
  const zipId = "i" + shortId.generate() + "d.zip"

  const basePath = join(tmpdir(), "store", dir)

  const zipFilePath = join(basePath, zipId)
  const archive = archiver("zip", {
    zlib: { level: 9 } // Sets the compression level.
  })
  const zipWriteStream = createWriteStream(zipFilePath)

  archive.pipe(zipWriteStream)

  uniqueNameImages.forEach(image => {
    archive.append(createReadStream(join(basePath, "images", image.fileIdName)), {
      name: image.uniqueRealName
    })
  })

  await archive.finalize()

  ctx.body = { zipId, dir }
})

export { router }
