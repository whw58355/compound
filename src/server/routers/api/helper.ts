import * as sharp from "sharp"
import { IOptions } from "../../../pages/index/store/interaface"
import { OutputInfo } from "sharp"
import { File } from "formidable"
import { join } from "path"

export function imageProcessor(
  options: IOptions,
  file: File,
  fileId: string,
  extension: string,
  outputDir: string
): {
  infoPromise: Promise<OutputInfo>
  fileIdName: string
} {
  // input
  let processor = sharp(file.path)
  let { quality, width, height, rotate, toFormat } = options

  const level = Math.floor(Number(quality) / 10) - 1
  const resizeWidth: number = width.trim() === "" ? null : Number(width)
  const resizeHeight: number = height.trim() === "" ? null : Number(height)

  // format and extension setting
  if (toFormat === "") {
    // keep origin extension
    toFormat = extension
  } else {
    // use the toFormat
    extension = toFormat
  }

  // common setting here
  processor
    .resize(resizeWidth, resizeHeight)
    .rotate(Number(rotate))
    .toFormat(toFormat)

  // compress
  if (toFormat == "jpeg" || toFormat === "jpg") {
    processor.jpeg({ quality: Number(quality) })
  }
  if (toFormat === "png") {
    processor.png({ compressionLevel: level })
  }
  if (toFormat === "webp") {
    processor.webp({ quality: Number(quality) })
  }
  if (toFormat === "tiff") {
    processor.tiff({ quality: Number(quality) })
  }

  //output
  const fileIdName = fileId + "." + extension
  const infoPromise = processor.toFile(join(outputDir, fileIdName))

  return { infoPromise, fileIdName }
}

export interface IImages {
  fileIdName: string
  fileName: string
  realName?: string
  realNameObj?:{
    name:string
    extension:string
  }
  uniqueRealName?: string
}

export function getUniqueNameImages(images: IImages[]): IImages[] {
  const imagesObj:any = {}
  images.forEach(image => {
    const extension = image.fileIdName.split(".").pop()
    const fileNameArr = image.fileName.split(".")
    fileNameArr.pop()
    const name = fileNameArr.join(".")
    const realName = name + "." + extension
    const obj = {...image,realName,realNameObj:{name,extension}}

    if(imagesObj[realName]){
      imagesObj[realName].push(obj)
    }else{
      imagesObj[realName] = [obj]
    }
  })

  const resultImages:IImages[] = []
  //
  for (let key in imagesObj) {
    if(! imagesObj.hasOwnProperty(key)) continue
     const imagesArr:IImages[] =  imagesObj[key]
    imagesArr.forEach((image,index)=>{
      const {name,extension} = image.realNameObj
      let uniqueRealName = name +"."+extension
      if(index>0) {
        uniqueRealName = name + `(${index}).`+extension
      }
      resultImages.push({...image,uniqueRealName})
    })

  }

  return resultImages
}
