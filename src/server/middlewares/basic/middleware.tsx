import * as Koa from "koa"
import { serverConfig } from "../../../config/server.config"
import {Context} from "koa"


 export async function handleNotFund(ctx: Koa.Context, next: Function) {
    await next()
    if (ctx.status === 404) {
      // do somthing here
      // ctx.redirect("/");
    }
  }

export function  handleError(app: Koa) {
    app.on("error", (error:Error,ctx:Context) => {

    if((/^maxFileSize exceeded/).test(error.message.trim())){
      console.log("in test")
      ctx.status = 500
      ctx.body = {msg:error.message}
      return
    }

      console.log("catch server error:", error)
    })
  }





