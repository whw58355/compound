import * as Koa from "koa"
import { resolve } from "path"
import { readdir } from "fs"
import { promisify } from "util"
const readdirPromisify = promisify(readdir)

// auto load routers in the routers dir
export async function autoUseRouters(app: Koa) {
  // for auto road all routers
  const routerPathArr = await readdirPromisify(resolve(__dirname + "/../routers"))

  for (let element of routerPathArr) {
    const { router } = await import(`../routers/${element}/router`)

    app.use(router.routes()).use(router.allowedMethods())
  }
}
