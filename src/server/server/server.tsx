import * as Koa from "koa"
import { handleError, handleNotFund } from "../middlewares/basic/middleware"
const app = new Koa()
import { serverConfig } from "../../config/server.config"
const { port } = serverConfig
import { autoUseRouters } from "./helper"
const koaBody = require('koa-body');


// body parser middleware
app.use(koaBody({multipart: true,formidable:{maxFileSize:10.5*1024*1024}}))



// handle 404
app.use(handleNotFund)

// auto load all routers from routers dir
autoUseRouters(app)

// handle all error here
handleError(app)

//  listen
app.listen(port)
