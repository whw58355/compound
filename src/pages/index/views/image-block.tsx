import * as React from "react"
import { Component, MouseEvent } from "react"
import { observer, inject } from "mobx-react"
import { action, observable } from "mobx"
import { IProps } from "../store/interaface"



@inject("store")
@observer
export class ImageBlock extends Component<IProps, {}> {
  processedImageBlock: HTMLDivElement
  @observable
  image = { processedWidth: 700, originWidth: 500, top: 0, left: 0 }
  data = { width: 1280, height: 853 }

  render() {
    const { processedWidth, originWidth } = this.image
    const vsBlockHeight = processedWidth * (this.data.height / this.data.width)
   // lang
    const {title} = this.props.store.lang.imageBlock

    return (
      <section className="image-block container">
        <h5 className="center-align">{title} </h5>

        <div
          style={{ height: vsBlockHeight + "px" }}
          className="vs-block"
          onMouseMove={this.handleMouseMove}
        >
          <div
            ref={ele => {
              this.processedImageBlock = ele
            }}
            className="processed-block"
          >
            <img className="z-depth-2" src="/public/images/processed.jpeg" alt="processed image" />
          </div>
          <div style={{ width: originWidth }} className="origin-block">
            <img
              className="z-depth-2"
              width={processedWidth}
              src="/public/images/origin.jpeg"
              alt="origin image"
            />
          </div>
          <div style={{ left: this.image.originWidth + "px" }} className="handler" />
          <div className="left-word"> 516 kb</div>
          <div className="right-word"> 254 kb</div>
        </div>
      </section>
    )
  }

  @action
  handleMouseMove = (e: MouseEvent<HTMLDivElement>) => {
    this.image.originWidth = e.clientX - this.image.left
  }

  componentDidMount() {
    this.handleClientChange()
   window.addEventListener("resize",this.handleClientChange)
  }

  componentWillUnmount(){
    window.removeEventListener("resize",this.handleClientChange)
  }

  @action
  handleClientChange = ()=>{

    const { width, top, left } = this.processedImageBlock.getBoundingClientRect()
    this.image.top = top
    this.image.left = left
    this.image.processedWidth = width
    this.image.originWidth = width / 2
  }

}
