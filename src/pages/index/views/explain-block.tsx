import * as React from "react"
import { inject } from "mobx-react"
import { Component } from "react"
import {IProps} from "../store/interaface"
@inject("store")
export class ExplainBlock extends Component<IProps> {
  render() {
    const {explainTitle1,explainP2,explainP3,explainTitle2,explainTitle3,explainP1} = this.props.store.lang.explainBlock
    return (
      <div className="container explain-block">
        <div className="row">
          <div className="col s12 l4">
            <h5>{explainTitle1}</h5>
            <p>{explainP1}</p>
          </div>
          <div className="col s12 l4">
            <h5>{explainTitle2}</h5>
            <p>{explainP2}</p>
          </div>
          <div className="col s12 l4">
            <h5>{explainTitle3}</h5>
            <p>
              {explainP3}
            </p>
          </div>
        </div>
      </div>
    )
  }
}
