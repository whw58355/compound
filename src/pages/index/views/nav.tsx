import * as React from "react"
import { Component } from "react"
import { observer, inject } from "mobx-react"
import { IProps } from "../store/interaface"
import {Dropdown} from "materialize-css"

@inject("store")
@observer
export class Nav extends Component<IProps, {}> {
  render() {
    const { url ,defaultLang} = this.props.store.lang.nav
    return (
      <section className="nav">
        <nav className="blue lighten-3 z-depth-0  ">
          <div className="nav-wrapper ">
            <div className="container ">
              <a href={url} className="brand-logo  ">
                <img className="logo-image" src="/public/images/logo.png" width={200} alt="" />
              </a>

              <div className="right hide-on-small-only">
                <a className="dropdown-trigger"  data-target="select-lang">
                  {defaultLang}
                  <i className="material-icons right">arrow_drop_down</i>
                </a>
              </div>
            </div>
          </div>
        </nav>

        <ul id="select-lang" className="dropdown-content">

          <li>
            <a href="/">English</a>
          </li>
          <li className="divider" />
          <li>
            <a href="/cn">中文</a>
          </li>
        </ul>
      </section>
    )
  }

  componentDidMount(){
    Dropdown.init(document.querySelectorAll(".dropdown-trigger"),{})
  }
}
