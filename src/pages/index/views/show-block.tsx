import * as React from "react"
import {inject} from "mobx-react"
import {Component} from "react"
import {IProps} from "../store/interaface"


@inject("store")
export class ShowBlock extends Component<IProps> {
  render(){
    const {h1,show1,show2} = this.props.store.lang.showBlock
    return (
      <div className="show-block container">
        <h1 className="center-align  hide">{h1} </h1>

        <p className="center-align explain">{show1}</p>

        <p className="center-align explain">
          {show2}
        </p>
      </div>
    )
  }
}


