import * as React from "react"
import {Component} from "react"
import {Modal} from "materialize-css"
import {inject} from "mobx-react"
import {IProps} from "../store/interaface"

@inject("store")
export class FooterBlock extends Component<IProps>{
  render(){
    let year = new  Date().getFullYear()
    // lang
    const {title ,p,close} = this.props.store.lang.contactMe

    return (

      <footer className="footer-block">
        <p className="center-align">© compress-image.net {year} All Rights Reserved. <a  className="contact-me modal-trigger"  href="#contact-me" >Contact me</a> </p>
        <div id="contact-me" className="modal contact-me">
          <div className="modal-content">
            <h4>{title}</h4>
            <p>{p}</p>
          </div>
          <div className="modal-footer">
            <a className="modal-close waves-effect waves-green btn-flat">{close}</a>
          </div>
        </div>


      </footer>
    )
  }

  componentDidMount(){
    Modal.init(document.querySelectorAll(".contact-me"),{})
  }
}

