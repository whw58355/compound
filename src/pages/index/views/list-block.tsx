import * as React from "react"
import { Component } from "react"
import { observer, inject } from "mobx-react"
import { IProps, IItem, IOptions } from "../store/interaface"
import * as prettyBytes from "pretty-bytes"
import * as shortid from "shortid"
import { Modal } from "materialize-css"
import { Store } from "../store/store"

@inject("store")
@observer
class ItemView extends Component<{ item: IItem }> {
  modalId = "i" + shortid.generate() + "d"
  render() {
    const { item } = this.props
    const { file, data } = item
    const { width, status, text } = item.progressBar
    const store: Store = (this.props as any).store
    const {
      download,
      moreInfo,
      afterProcessed,
       width:langWidth,
      height,
      size,
      saved,
      format,
      close
    } = store.lang.listBlock

    // after success data
    let afterSize = 0
    let reduceRate = "-"
    let downloadUrl = ""

    // only success
    if (data) {
      const {
        fileIdName,
        dirName,
        info: { size, format }
      } = data

      afterSize = size
      let reduceRateNumber = Math.floor(((size - file.size) / file.size) * 100)
      let plus = "-"
      if (reduceRateNumber > 0) {
        plus = "+"
      }

      const originFileName = encodeURI(file.name)
      reduceRate = `${plus} ${Math.abs(reduceRateNumber)} %`
      downloadUrl = `/api/images/${dirName}/${fileIdName}?origin=${originFileName}`
    }

    const barClassName = `truncate my-bar my-bar-${status}`

    //bar width only for uploading
    const barWidthStyle = { width: width }

    // hide the info until success
    let visibility: any = "hidden"

    if (status === "finished") {
      visibility = "visible"
    }

    // render data

    let fileName = file.name
    let prettyFileSize = prettyBytes(file.size)
    let prettyAfterSize = prettyBytes(afterSize)
    let toFormat = ""
    let afterWidth = null
    let afterHeight = null

    if (data) {
      toFormat = data.info.format
      afterWidth = data.info.width
      afterHeight = data.info.height
    }

    //item to return
    return (
      <div>
        <div className="item-block">
          <div className="item-left truncate">{fileName}</div>

          <div className="item-center">
            <div className="item-center-before">{prettyFileSize}</div>

            <div className="item-center-main">
              <div className="my-progress">
                <div style={barWidthStyle} className={barClassName}>
                  {text}
                </div>
              </div>
            </div>

            <div style={{ visibility: visibility }} className="item-center-after">
              {prettyAfterSize}
            </div>
          </div>

          <div className="item-right truncate " style={{ visibility: visibility }}>
            <div className="reduce-rate">{reduceRate}</div>
            <div>
              <a className="download-single" href={downloadUrl}>
                {download}
              </a>
            </div>

            <div className="icon-more">
              <a className="modal-trigger more-options-icon" href={"#" + this.modalId}>
                <i className=" material-icons">more_vert</i>
              </a>
            </div>
          </div>
        </div>

        <div id={this.modalId} className="modal">
          <div className="modal-content">
            <h4>{moreInfo}</h4>
            <p className="modal-content-subtitle">{afterProcessed}</p>

            <table className="highlight  responsive-table">
              <tbody>
                <tr>
                  <td>
                    <div className="table-td-first-div">{size}</div>
                    <div>{prettyAfterSize}</div>
                  </td>

                  <td>
                    <div className="table-td-first-div">{saved}</div>
                    <div>{reduceRate}</div>
                  </td>
                </tr>

                <tr>
                  <td>
                    <div className="table-td-first-div">{langWidth}(px):</div>
                    <div>{afterWidth}</div>
                  </td>
                  <td>
                    <div className="table-td-first-div">{height}(px):</div>
                    <div>{afterHeight}</div>
                  </td>
                </tr>

                <tr>
                  <td>
                    <div className="table-td-first-div">{format}:</div>
                    <div>{toFormat}</div>
                  </td>
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
          <div className="modal-footer">
            <a className="modal-close waves-effect waves-green btn-flat">{close}</a>
          </div>
        </div>
      </div>
    )
  }

  componentDidMount() {
    Modal.init(document.querySelectorAll("#" + this.modalId), {})
  }
}

@inject("store")
@observer
export class ListBlock extends Component<IProps, {}> {
  render() {
    const store = this.props.store
    const items = store.imageItems.map((item, index) => <ItemView item={item} key={index} />)
    // if item <= 0 ,hide the whole container
    const show = items.length > 0 ? "" : "hide"

    return <section className={`list-block container  ${show}`}>{items}</section>
  }
}
