import * as React from "react"
import { Component } from "react"
import { observer, inject } from "mobx-react"
import { IProps } from "../store/interaface"
import { Nav } from "./nav"
import { UploadBlock } from "./upload-block"
import { ListBlock } from "./list-block"
import { ImageBlock } from "./image-block"
import { OptionsBlock } from "./options-block"
import { ShowBlock } from "./show-block"
import { ExplainBlock } from "./explain-block"
import {FooterBlock} from "./footer-block"
@inject("store")
@observer
export class Main extends Component<IProps, {}> {
  render() {
    return (
      <div >
        <Nav />
        <ShowBlock />

        <OptionsBlock />
        <UploadBlock />

        <ListBlock />
        <ImageBlock />
        <ExplainBlock />
        <FooterBlock/>
      </div>
    )
  }
}
