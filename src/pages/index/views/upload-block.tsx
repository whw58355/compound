import * as React from "react"
import { Component, ChangeEvent } from "react"
import { observer, inject } from "mobx-react"
import { IItem, IProps, ProgressBar } from "../store/interaface"
import { action, runInAction } from "mobx"
import axios from "axios"
import { Tooltip } from "materialize-css"

@inject("store")
@observer
export class UploadBlock extends Component<IProps, {}> {
  inputElement: HTMLInputElement

  copyFiles = (files: FileList): File[] => {
    const fileArr: File[] = []

    for (let i = 0; i < files.length; i++) {
      fileArr.push(files[i])
    }

    return fileArr
  }

  handleInputFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
    // should copy the files first ,the event.files was trashed after update
    const files = this.copyFiles(e.target.files)
    await this.addFiles(files)
  }

  handleDownloadAllClick = () => {
    // do nothing when have jobs remain
    if (this.props.store.jobsRemain > 0) return

    // do nothing when there are no images
    if (this.props.store.imageItems.length === 0) return

    this.downloadAll()
  }

  downloadAll = async () => {
    const { id } = this.props.store.options
    const images = this.props.store.imageItems
      .filter(item => {
        return item.data !== null ? true : false
      })
      .map(item => {
        const {
          data: { fileIdName },
          file
        } = item
        return { fileIdName, fileName: file.name }
      })

    const { status, data } = await axios({
      url: `/api/zip/${id}`,
      method: "post",
      data: { images }
    })

    if (status === 200) {
      window.open(`/api/zip/${data.dir}/${data.zipId}`, "_self")
    }
  }

  @action.bound
  async addFiles(files: File[]) {
    const { options, imageItems } = this.props.store
    const myOptions = JSON.parse(JSON.stringify(options))

    // lang
    const { uploadPending } = this.props.store.lang.uploadBlock.listText

    this.props.store.jobsRemain += files.length

    const uploadActionArr: Function[] = []

    for (let i = 0; i < files.length; i++) {
      const file = files[i]

      const progressBar: ProgressBar = {
        status: "waiting",
        text: uploadPending,
        width: "100%"
      }

      const obj: IItem = { file, progressBar, data: null }

      imageItems.push(obj)

      const currentItem = imageItems[imageItems.length - 1]

      uploadActionArr.push(async () => {
        await this.uploadOne(file, currentItem, myOptions)
        runInAction(() => this.props.store.jobsRemain--)
      })
    }

    for (let uploadAction of uploadActionArr) {
      await uploadAction()
    }
  }

  @action.bound
  async uploadOne(file: File, currentItem: IItem, options: object) {
    const { progressBar } = currentItem

    // lang
    const { uploading, compressing, finished, error } = this.props.store.lang.uploadBlock.listText

    try {
      const formData = new FormData()
      formData.append("file", file)

      const { status, data } = await axios.post("/api/compress", formData, {
        params: options,
        headers: { "Content-Type": "multipart/form-data" },
        onUploadProgress: action((progressEvent: ProgressEvent) => {
          const { loaded, total } = progressEvent
          const barWidth = Math.floor((loaded / total) * 100)

          progressBar.status = "uploading"
          progressBar.text = uploading
          progressBar.width = barWidth + "%"

          if (barWidth === 100) {
            progressBar.status = "compressing"
            progressBar.text = compressing
          }
        })
      })

      if (status === 200) {
        runInAction(() => {
          progressBar.status = "finished"
          progressBar.text = finished
          progressBar.width = "100%"
          currentItem.data = data
        })
      } else {
        throw new Error("not finished")
      }
    } catch (e) {
      runInAction(() => {
        progressBar.status = "error"
        progressBar.text = error
        progressBar.width = "100%"
      })
    }
  }

  render() {
    const {
      upload,
      uploadTooltip,
      downloadAll,
      downloadAllTooltip,
      waiting
    } = this.props.store.lang.uploadBlock
    const { jobsRemain, imageItems } = this.props.store
    const downloadCount = imageItems.filter(item => {
      if (item.data) {
        return true
      } else {
        false
      }
    }).length

    let downloadButtonText = `${downloadAll} (${downloadCount})`
    if (jobsRemain > 0) {
      downloadButtonText = `${waiting} (${jobsRemain})`
    }

    return (
      <section className="upload-block container">
        <input
          className="hide"
          accept=".jpg, .jpeg, .png, .webp, .tiff"
          onChange={this.handleInputFileChange}
          ref={ele => {
            this.inputElement = ele
          }}
          value={""}
          type="file"
          multiple={true}
        />

        <div className="button-block center-align">
          <button
            onClick={() => {
              this.inputElement.click()
            }}
            data-position="bottom"
            data-tooltip={uploadTooltip}
            className="tooltipped first-button waves-effect waves-light btn light-green lighten-1"
          >
            {upload}
          </button>
          &nbsp;&nbsp;
          <button
            onClick={this.handleDownloadAllClick}
            data-position="bottom"
            data-tooltip={downloadAllTooltip}
            className="tooltipped  waves-effect waves-light btn  blue lighten-1"
          >
            {downloadButtonText}
          </button>
          &nbsp;&nbsp;
        </div>
      </section>
    )
  }

  handleFileDrop = (e: DragEvent) => {
    e.preventDefault()
    const { files } = e.dataTransfer

    // copy and filter
    const filesArr: File[] = []
    for (let i = 0; i < files.length; i++) {
      const file = files[i]
      const extension = file.name.split(".").pop()
      if (
        extension === "jpg" ||
        extension === "jpeg" ||
        extension === "png" ||
        extension === "webp" ||
        extension === "tiff"
      ) {
        filesArr.push(files[i])
      }
    }

    this.addFiles(filesArr)
  }

  handleFileDragover = (e: DragEvent) => {
    e.preventDefault()
  }

  componentDidMount() {
    Tooltip.init(document.querySelectorAll(".tooltipped"), {})

    window.addEventListener("dragover", this.handleFileDragover)

    window.addEventListener("drop", this.handleFileDrop)
  }

  componentWillUnmount() {
    window.removeEventListener("dragover", this.handleFileDragover)
    window.removeEventListener("drop", this.handleFileDrop)
  }
}
