import * as React from "react"
import { Component, ChangeEvent } from "react"
import { observer, inject } from "mobx-react"
import { IProps } from "../store/interaface"
import { Store } from "../store/store"
import { action } from "mobx"
import { Tabs } from "materialize-css"

@inject("store")
@observer
export class OptionsBlock extends Component<IProps, {}> {
  @action
  handleQualityChange = (e: ChangeEvent<HTMLSelectElement>) => {
    this.props.store.options.quality = e.target.value
  }

  @action
  handleFormatChange = (e: ChangeEvent<HTMLSelectElement>) => {
    this.props.store.options.toFormat = e.target.value
  }

  @action
  handleRotateChange = (e: ChangeEvent<HTMLSelectElement>) => {
    this.props.store.options.rotate = e.target.value
  }

  @action
  handleWidthOrHeightChange = (e: ChangeEvent<HTMLInputElement>) => {
    const store: Store = this.props.store
    const options = store.options as any
    const { name, value } = e.target
    options[name] = value
  }

  getQualityContent = () => {
    const {label,recommend} = this.props.store.lang.optionsBlock.quality
    const qualityOptions: any[] = []
    for (let i = 100; i >= 30; i = i - 5) {
      if (i === 65) {
        qualityOptions.push(
          <option value={i} key={"key" + i}>
            {i + `    ( ${recommend} )`}
          </option>
        )
        continue
      }
      qualityOptions.push(
        <option value={i} key={i}>
          {i}
        </option>
      )
    }

    const quality = (
      <div className="row">
        <div className="col s12 m6 l3">
          <label className="label-field">{label} (%)</label>
          <select
            defaultValue={"65"}
            className="browser-default"
            onChange={this.handleQualityChange}
          >
            {qualityOptions}
          </select>
        </div>
      </div>
    )

    return quality
  }

  getResizeContent = () => {
    const store: Store = this.props.store
    const { width, height } = store.options
    const {width:langWidth,height:langHeight,auto} = this.props.store.lang.optionsBlock.resize
    return (
      <div className="row">
        <div className="col s12 m6 ">
          <div className="input-field col s12">
            <input
              id="width"
              placeholder={auto}
              type="number"
              onChange={this.handleWidthOrHeightChange}
              name="width"
              min={1}
              max={100000}
              value={width}
              className="validate"
            />
            <label htmlFor="width" className="label-field">
              {langWidth} (px)
            </label>
          </div>
        </div>

        <div className="col s12 m6 ">
          <div className="input-field col s12">
            <input
              id="height"
              placeholder={auto}
              type="number"
              onChange={this.handleWidthOrHeightChange}
              name="height"
              min={1}
              max={100000}
              value={height}
              className="validate"
            />
            <label htmlFor="height" className="label-field">
              {height} (px)
            </label>
          </div>
        </div>
      </div>
    )
  }

  getRotateContent = () => {
   const {label,origin} = this.props.store.lang.optionsBlock.rotate
    return (
      <div className="row">
        <div className="col s12 m6 l3">
          <label className="label-field">{label} (deg)</label>
          <select defaultValue={"0"} className="browser-default" onChange={this.handleRotateChange}>
            <option value="0">{origin}</option>
            <option value="90">90</option>
            <option value="180">180</option>
            <option value="270">270</option>
          </select>
        </div>
      </div>
    )
  }

  getToFormatContent = () => {
   const {label,origin} = this.props.store.lang.optionsBlock.format
    return (
      <div className="row">
        <div className="col s12 m6 l3">
          <label className="label-field">{label}</label>
          <select defaultValue={"0"} className="browser-default" onChange={this.handleFormatChange}>
            <option value="">{origin}</option>
            <option value="jpg">jpg</option>
            <option value="jpeg">jpeg</option>
            <option value="png">png</option>
            <option value="webp">webp</option>
            <option value="tiff">tiff</option>
          </select>
        </div>
      </div>
    )
  }

  render() {
    const qualityContent = this.getQualityContent()
    const resizeContent = this.getResizeContent()
    const rotateContent = this.getRotateContent()
    const formatContent = this.getToFormatContent()
    const {quality,resize,rotate,format} = this.props.store.lang.optionsBlock.navigation

    return (
      <section className="options-block container">
        <ul className="tabs  ">
          <li className="tab">
            <a className="active" href="#quality">
              {quality}
            </a>
          </li>
          <li className="tab">
            <a href="#resize">{resize}</a>
          </li>
          <li className="tab">
            <a href="#format">{format}</a>
          </li>

          <li className="tab">
            <a href="#rotate">{rotate}</a>
          </li>


        </ul>

        <div id="quality" className="tab-content col s12">
          {qualityContent}
        </div>
        <div id="resize" className="tab-content resize-content col s12">
          {resizeContent}
        </div>

        <div id="format" className="tab-content col s12">
          {formatContent}
        </div>

        <div id="rotate" className="tab-content col s12">
          {rotateContent}
        </div>



      </section>
    )
  }

  componentDidMount(){
    Tabs.init(document.querySelectorAll(".tabs"), {})
  }

}
