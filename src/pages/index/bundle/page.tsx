import { Main } from "../views/main"

// for module hot replacement
import { hot } from "react-hot-loader"

// this page only for export page for ssr and bundle

export const Page = hot(module)(Main)
