import * as React from "react"
import { Page } from "./page"
import { Store } from "../store/store"
const storeFromServer = (window as any).__store
import { Provider } from "mobx-react"
import { hydrate, render } from "react-dom"

// import scss here , only for client
import "../scss/page.scss"

const store = new Store()

// set lang
const langFromServer = (window as any).lang
if (langFromServer) store.lang = langFromServer

const root = document.getElementById("root")

const element = (
  <Provider store={store}>
    <Page />
  </Provider>
)

// for ssr and dev in webpack dev server
if (root.childNodes.length > 0) {
  hydrate(element, root)
} else {
  render(element, root)
}
