import { ILang } from "./interface"
export const lang: ILang = {

  head: {
    lang: "zh-CN",
    title: "压缩图片",
    description: "一个功能强大的在线图片压缩工具。功能包括：图片压缩，调整图片大小，转换格式，旋转等。"
  },

  nav:{
    url:"/cn",
    defaultLang:"中文"
  },

  showBlock: {
    h1: "压缩图片",
    show1:
      "一个功能强大的在线图片压缩工具。功能包括：图片压缩，调整图片大小，转换格式，旋转等。",
    show2:
      "把图片直接拖进页面，或者点击上传，选择最多20张图片, 等待处理完成。 "

  },

  optionsBlock: {
    navigation:{
      quality: "压缩等级",
      resize: "调整大小",
      format: "格式",
      rotate: "旋转",
    },

    quality:{
      label:"压缩等级",
      recommend: "推荐",
    },
    resize: {
      width:"宽",
      height:"高",
      auto:"自动"
    },
    format: {
      label:"转换格式",
      origin:"原始格式"
    },
    rotate: {
      label:"旋转",
      origin:"无"
    },


  },

  uploadBlock: {
    upload: "上传",
    downloadAll: "下载所有",
    uploadTooltip: "先选择合适的选项，默认选项仅仅压缩图片。",
    downloadAllTooltip: "下载包含所有图片的压缩包。",
    waiting: "等待",
    listText: {
      uploadPending: "等待上传...",
      compressing: "正压缩",
      finished: "完成",
      error: "出错了",
      uploading:"正上传..."
    }
  },
  imageBlock: {
    title: "压缩前 vs 压缩后"
  },

  listBlock: {
    moreInfo: "更多信息",
    afterProcessed: "处理后",
    size: "大小",
    saved: "节省",
    width: "宽",
    height: "高",
    download: "下载",
    format: "格式",
    close: "关闭"
  },

  explainBlock: {
    explainTitle1: "文件格式",
    explainTitle2: "使用",
    explainTitle3: "浏览器支持",
    explainP1: "你可以上传最多20张图片 (最大 10 MB 每张 ) ，格式可以是： JPG,JPEG, PNG, WebP, TIFF。",
    explainP2:
      "此工具可以对图片进行压缩，转换格式，调整大小，旋转等操作。压缩和调整图片大小（设置合适的宽和高），可以极大地减少图片的大小。" ,

    explainP3:
      "完美兼容所有现代浏览器，如果有问题可以联系我。"
  },
  contactMe:{
    title:"联系我",
    p:"我的gmail账号: whw58355",
    close:"关闭"
  }
}
