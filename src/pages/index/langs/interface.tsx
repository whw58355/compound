export interface ILang {
  head: {
    lang: string
    title: string
    description: string
  }

  nav: {
    url: string
    defaultLang:string
  }

  showBlock: {
    h1: string
    show1: string
    show2: string
  }

  optionsBlock: {
    navigation: {
      quality: string
      resize: string
      format: string
      rotate: string
    }

    quality: {
      label: string
      recommend: string
    }
    resize: {
      width: string
      height: string
      auto: string
    }
    format: {
      label: string
      origin: string
    }
    rotate: {
      label: string
      origin: string
    }
  }

  uploadBlock: {
    upload: string
    downloadAll: string
    uploadTooltip: string
    downloadAllTooltip: string
    waiting: string
    listText: {
      uploading: string
      uploadPending: string
      compressing: string
      finished: string
      error: string
    }
  }

  listBlock: {
    download: string
    moreInfo: string
    afterProcessed: string
    width: string
    height: string
    size: string
    saved: string
    format: string
    close: string
  }
  imageBlock: {
    title: string
  }

  explainBlock: {
    explainTitle1: string
    explainTitle2: string
    explainTitle3: string
    explainP1: string
    explainP2: string
    explainP3: string
  }

  contactMe: {
    title: string
    p: string
    close: string
  }
}
