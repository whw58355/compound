import { ILang } from "./interface"
export const lang: ILang = {

  head: {
    lang: "en",
    title: "Compress Image - Reduce image size online",
    description: "A powerful online tool for image compression, resizing, reformatting, rotating and more."
  },
  nav:{
    url:"/",
    defaultLang:"English"
  },


  showBlock: {
    h1: "Compress Image - Reduce image size online",
    show1:
      "A powerful online tool for image compression,resizing, reformatting, rotating and more.",
    show2:
      "Drag files to anywhere in this page, or select up to 20 images by using upload button. Wait for the procession to finish."
  },

  optionsBlock: {
    navigation:{
      quality: "quality",
      resize: "resize",
      format: "format",
      rotate: "rotate",
    },

    quality:{
      label:"compress quality",
      recommend: "recommend",
    },
    resize: {
      width:"width",
      height:"height",
      auto:"auto"
    },
    format: {
      label:"to format",
      origin:"origin format"
    },
    rotate: {
      label:"rotate",
      origin:"origin"
    },


  },

  uploadBlock: {
    upload: "upload",
    downloadAll: "download all",
    uploadTooltip: " Select your options first. Default value only compress images.",
    downloadAllTooltip: " Download all processed images grouped in a zip archive.",
    waiting: "waiting",
    listText: {
      uploadPending: "upload pending...",
      compressing: "compressing",
      finished: "finished",
      error: "error happened",
      uploading:"uploading..."
    }
  },
  imageBlock: {
    title: "origin vs compressed"
  },

  listBlock: {
    moreInfo: "More Info",
    afterProcessed: "after processed",
    size: "size",
    saved: "saved",
    width: "width",
    height: "height",
    download: "download",
    format: "format",
    close: "close"
  },

  explainBlock: {
    explainTitle1: "File Formats",
    explainTitle2: "Usage",
    explainTitle3: "Browser Support",
    explainP1: "You can upload up to 20 images (max. 10 MB each) as JPG,JPEG, PNG, WebP, TIFF.",
    explainP2:
      "You can use this tool to compress, reformat, resize,rotate your images. Compress and resize image(set the width or height ) can dramatically reduce your file size.",
    explainP3:
      "Work very well on all modern browser including mobile devices.Contact me if there are issues."
  },
  contactMe:{
    title:"Contact Me",
    p:"My gmail Account: whw58355",
    close:"close"
  }

}
