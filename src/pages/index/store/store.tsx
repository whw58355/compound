import { observable, action, computed ,runInAction,configure } from "mobx"
import { IStore, ProgressBar, IItem } from "./interaface"
import * as  shorid from "shortid"
const id = "i"+shorid.generate()+"d"
configure({enforceActions:"observed"})
import {lang} from "../langs/en"
import {ILang} from "../langs/interface"

class Store implements IStore {
  lang:ILang = lang

  @observable
  options = {
    id:id,
    quality: "65",
     width: "",
    height: "" ,
    rotate:"0",
    toFormat:""
  }

  @observable
  jobsRemain = 0

  @observable
  imageItems:IItem[] = []


}

export { Store }
