import { Store } from "./store"
import { ICommonStore } from "../../../common/store/common"

export interface IProps {
  store?: Store
}

interface OutputInfo {
  format: string
  size: number
  width: number
  height: number
  channels: number
}

export interface Data {
  fileIdName: string
  dirName: string
  info: OutputInfo
}

export type ProgressBar = {
  status: "waiting" | "uploading" | "compressing" | "finished" | "error"
  text: string
  width: string
}

export interface IOptions {
  readonly id: string
  quality: string
  width: string
  height: string
  rotate: string
  toFormat: string
}

export interface IItem {
  file: File
  progressBar: ProgressBar
  data: null | Data
}

export interface IStore extends ICommonStore {
  options: IOptions
  jobsRemain:number

  imageItems: IItem[]
}
