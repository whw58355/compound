import { shareConfig } from "./share.config"
const serverConfig = {
  shareConfig: shareConfig,
  poolSize: 1,
  mongoURL:"mongodb://localhost:27017",
  mongoDbName:"compress",
  port: 3000
}

// production config here
if (process.env.NODE_ENV === "production") {
  serverConfig.poolSize = 10
}

export { serverConfig }
