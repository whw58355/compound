import { shareConfig } from "./share.config"
const clientConfig = {
  shareConfig: shareConfig
}

export { clientConfig }
