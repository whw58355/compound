import * as puppeteer from "puppeteer"
import { Browser } from "puppeteer"
import { join } from "path"
import * as makeDir from "make-dir"
import { writeFile } from "fs"
import { promisify } from "util"
import * as cheerio from "cheerio"

const ga = `<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127181720-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127181720-1');
</script>
`
const adverify = `<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6380335858502820",
    enable_page_level_ads: true
  });
</script>`


function injectScript(html: string):string {
  const $ = cheerio.load(html)
  const isProduction = $("#is-production").length

  // remove useless script
  $("script#main-script").nextAll().remove()


  // only in production inject ga
  if(isProduction>0){
    $("body").append(ga)
    $("head").append(adverify)
  }

  return $.html()

}

async function generate(name: string, browser: Browser) {
  const page = await browser.newPage()
  await page.goto(`http://localhost:3000/ssr/${name}`)
  const htmlDirPath = join(__dirname, "../../../static/build/html")

  await makeDir(htmlDirPath)
  const html = await page.content()

  const withScriptHtml = injectScript(html)

  await promisify(writeFile)(htmlDirPath + `/${name}.html`, withScriptHtml)
}

;(async () => {
  console.time("puppeteer")
  const browser = await puppeteer.launch({
    headless: true,
    args: ["--no-sandbox"]
  })

  //
  const nameArr = ["en", "cn"]

  // generate htmls
  for (let name of nameArr) {
    await generate(name, browser)
  }

  await browser.close()
  console.timeEnd("puppeteer")
})()


