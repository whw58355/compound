import { Entry } from "webpack"
import { promisify } from "util"
import { readdir, writeFile } from "fs"
import { resolve, join } from "path"

export interface Env {
  who?: string | undefined
  dev?: boolean | undefined
  mode?: string | undefined
}

export async function getEntry(env: undefined | Env) {
  let entry: any = {}

  if (env && env.who) {
    entry = { [env.who]: `./src/pages/${env.who}/bundle/bundle.tsx` }
  } else {
    const entryDirs = await promisify(readdir)(join(__dirname, "../../pages"))
    for (let dir of entryDirs) {
      entry[dir] = `./src/pages/${dir}/bundle/bundle.tsx`
    }
  }

  return entry
}

export async function createDevHtml(env: Env) {
  // create index.html file for dev in ./build/html
  let who = "index"

  if (env.who) {
    who = env.who
  }

  const html = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${who}</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<body>
<div id="root"></div>
<script  src="/build/bundle/${who}.bundle.js"></script>
</body>
</html>`

  await promisify(writeFile)(join(__dirname, "/html/index.html"), html)
}

export function getDevEntry(env: Env) {
  let who = "index"

  if (env.who) {
    who = env.who
  }

  let entry = { [who]: `./src/pages/${who}/bundle/bundle.tsx` }
  return entry
}
