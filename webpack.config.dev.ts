import { Env } from "./src/scripts/webpack/helper"
import { Configuration, HotModuleReplacementPlugin } from "webpack"
import { resolve, join } from "path"
import { createDevHtml, getDevEntry } from "./src/scripts/webpack/helper"
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin")
import base from "./webpack.config.base"

export default async (env: Env) => {
  await createDevHtml(env)

  const config: Configuration = {
    ...base,
    entry: getDevEntry(env),
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          include: resolve(__dirname, "src"),
          use: [
            {
              loader: "babel-loader",
              options: {
                babelrc: false,
                cacheDirectory: true,
                plugins: ["react-hot-loader/babel"]
              }
            },

            {
              loader: "ts-loader",
              options: {
                transpileOnly: true,
                experimentalWatchApi: true,
                configFile: "tsconfig.webpack.dev.json"
              }
            }
          ]
        },
        {
          test: /\.scss$/,
          // exclude: /node_modules/,
          // include: resolve(__dirname, "src"),
          use: ["style-loader", "css-loader", "sass-loader"]
        },
        {
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          loader: "url-loader?limit=30000&name=[name]-[hash].[ext]"
        }
      ]
    },
    plugins: [
      new HotModuleReplacementPlugin(),
      new ForkTsCheckerWebpackPlugin({
        checkSyntacticErrors: true,
        tsconfig: resolve(__dirname, "./tsconfig.webpack.dev.json"),
        workers: 2,
        watch: ["./src/**/*"]
      })
    ]
  }

  const configAny = config as any

  configAny.devServer = {
    contentBase: [
      join(__dirname, "src/scripts/webpack/html"),

    ],
    index: "index.html",
    hot: true,
    inline: true,
    open: true,
    overlay: true,
    compress: false,
    proxy: {
      "/api": "http://localhost:3000",
      "/public": "http://localhost:3000",
      "/favicon.ico": "http://localhost:3000",
    }
  }

  return config
}
