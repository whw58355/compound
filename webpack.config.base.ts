import {Configuration} from "webpack"

const config:Configuration={
  output: {
    filename: "[name].bundle.js",
    path: __dirname + "/static/build/bundle/",
    publicPath: "/build/bundle",
    pathinfo: false
  },

  // Enable sourcemaps for debugging webpack's output.
  devtool: "cheap-module-eval-source-map",

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".json"]
  }

}

export default config